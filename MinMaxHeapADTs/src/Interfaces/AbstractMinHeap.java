package Interfaces;
/*
 * The AbstractMinHeap interface contains method to access minimum heap operations
 */
public interface AbstractMinHeap<T> {
	/*
     * Adds the item into a heap 
     */
	public void add(T item);
	 /*
     * Removes the minimum element of a heap
     * @return Removed item
     */
	public T deleteMin();
	/*
	 * Clears the heap items
	 */
	public void clear();
	/*
	 * Returns First element of heap
	 * @returns first element of heap
	 */
	public T first();
	/*
     * Iterate over the elements of heap by successive calls
     * Remember to call first() before calling this method
     * @return Elements in an array at each position, Null if heap is empty
     */
	public T next();
}
