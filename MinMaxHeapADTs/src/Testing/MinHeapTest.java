package Testing;
import java.util.Random;

import org.junit.Test;

import HeapClasses.MinHeap;
import Interfaces.AbstractMinHeap;
/*
 * Represent the test class for Min Heap implementation 
 */
public class MinHeapTest {
	 /*
     * Test method to test the min heap implementation through random insert
     */
	@Test
	public void randomInsertTest() {
		AbstractMinHeap<Integer> minHeap = new MinHeap<>(30);
		Random random = new Random(System.currentTimeMillis());
		System.out.println("The Random No are :");
		for (int i = 0; i < 30; i++) {	
			int randomNo = Math.round(random.nextInt(100));	
			System.out.print(randomNo);
			System.out.print(", ");
			minHeap.add(randomNo);
		}
		System.out.println("");
		System.out.println("The Deleted Elements according to deletion order are :");
		for (int i = 0; i < 30; i++) {			
			System.out.print(minHeap.deleteMin());
			System.out.print(", ");
		}
		System.out.println("");
		System.out.println("Thus, the elements are deleted in their ascending order with the smallest element deleted first.");
		//The deleted items are displayed in the console in descending order
		assert(true);
	}
}
