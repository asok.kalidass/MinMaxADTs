package Testing;
import java.util.Random;
import org.junit.Test;
import HeapClasses.Heap;
import Interfaces.AbstractMaxHeap;
/*
 * Represent the test class for Max Heap implementation 
 */
public class MaxHeapTest {
    /*
     * Test method to test the max heap implementation
     */
	@Test
	public void randomIntegerInsertTest() {
		AbstractMaxHeap<Integer> maxHeap = new Heap<>(30);
		Random random = new Random(System.currentTimeMillis());
		System.out.println("The generated Random No are :");
		for (int i = 0; i < 30; i++) {	
			int randomNo = Math.round(random.nextInt(100));	
			System.out.print(randomNo);
			System.out.print(", ");
			maxHeap.add(randomNo);
		}
		System.out.println("");
		System.out.println("The Heap nodes after insertion is :");
		maxHeap.first();
		while (maxHeap.next() != null) {
			System.out.print(maxHeap.next());
			System.out.print(", ");
		}
		System.out.println("");
		System.out.println("The Deleted Elements according to deletion order are :");
		for (int i = 0; i < 30; i++) {			
			System.out.print(maxHeap.deleteMax());
			System.out.print(", ");
		}
		System.out.println("");
		System.out.println("Thus, the elements are deleted in their descenting order with the largest element deleted first.");
		//The deleted items are displayed in the console in descending order
		assert(true);
	}	
}
