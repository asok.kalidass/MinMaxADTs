package HeapClasses;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import Interfaces.AbstractMinHeap;

/**
 * This class implements a generic min heap.
 * @author Asok Kalidass Kalisamy (B00763356)
 *
 * @param <T> Type of the heap structure to store data
 */
public class MinHeap<T extends Comparable<T>> implements AbstractMinHeap<T> {
    //Declarations
	ArrayList<T> items;

	int position;
    /*
     * Empty initialization of heap
     */
	public MinHeap() {
		this.items = new ArrayList<>();
		position = -1;
	}
    /*
     * Initialization of heap 
     */
	public MinHeap(int size) {
		this.items = new ArrayList<>(size);
		position = -1;
	}
    /*
     * Rearranges the structure from bottom such that parent is lesser thatn its children
     */
	void shiftUp(int index) {
		T insertedItem = items.get(index);
		while(index > 0) {
			int indexOfParent = (index - 1) / 2;
			T parentOfInsertedItem = items.get(indexOfParent);
			if (insertedItem.compareTo(parentOfInsertedItem) < 0) {
				items.set(index, parentOfInsertedItem);
				index = indexOfParent;
			}
			else {
				break;
			}
		}
		items.set(index, insertedItem);
	}
    /*
     * Adds the item into a heap 
     */
	public void add(T item) {
		items.add(item);
		shiftUp(items.size() - 1);
	}
    /*
     * Shifts down the item after deletion from top of structure 
     * @param item - start index of an array
     */
	void shiftDown(int index) {		
		T root = items.get(index);
		int leftChildIndex = 2 * index + 1;
		while (leftChildIndex < items.size()) {
			T leftChild = items.get(leftChildIndex);
			int leftIndex = leftChildIndex;
			//compare the children and get the smallest one 
			int rightChildIndex = leftChildIndex + 1;
			if (rightChildIndex < items.size()) {
				T rightChild = items.get(rightChildIndex);
				if (rightChild.compareTo(leftChild) < 0) {
					leftChild = rightChild;
					leftIndex = rightChildIndex;
				}
			}
			//compare the smallest child with root and swap it root is bigger 
			if (leftChild.compareTo(root) < 0) {
				items.set(index, leftChild);
				index = leftIndex;
				leftChildIndex = 2 * index + 1;
			}
			else break;
		}
		items.set(index, root);
	}
    /*
     * Removes the minimum element of an array and rearranges the structure
     * @return Removed item
     */
	public T deleteMin() {
		if (items.isEmpty()) {
			throw new NoSuchElementException();
		}
		
		T itemToBeRemoved = items.get(0);
		T maxItem = items.remove(items.size() - 1);

		if(items.isEmpty()) {
			return itemToBeRemoved;
		}

		items.set(0, maxItem);

		shiftDown(0);

		return itemToBeRemoved;
	}
	/*
	 * Clears the heap items
	 */
	public void clear() {
		items.clear();
	}
	/*
	 * Returns First element of heap
	 * @returns first element of heap
	 */
	public T first() {
		if (items.size() == 0) return null;
		position = 0;
		return items.get(position);
	}
    /*
     * Iterate over the elements of heap by successive calls
     * Remember to call first() before calling this method
     * @return Elements in an heap at each position, Null if heap is empty
     */
	public T next() {
		if (position < 0 || position == (items.size() - 1)) {
			return null;
		}
		position++;
		return items.get(position);
	}
}
