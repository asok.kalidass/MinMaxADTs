
********************************************************************************************************************       
 Heaps:

   Description: Min and Max Heap ADT's

   Files: 
    Heap.java
    MinHeap.java
    AbstractMaxHeap.java
    AbstractMinHeap.java

   Run instructions: 
     MaxHeapTest.java
     MinHeapTest.java

   Output :
     
     MaxHeap:
 
     The generated Random No are :
3, 60, 33, 95, 77, 77, 71, 93, 31, 63, 80, 33, 94, 52, 87, 91, 39, 79, 12, 40, 23, 50, 1, 99, 20, 59, 69, 64, 53, 23, 
     The Heap nodes after insertion is :
95, 80, 87, 79, 63, 69, 71, 39, 12, 23, 1, 20, 59, 53, null, 
     The Deleted Elements according to deletion order are :
99, 95, 94, 93, 91, 87, 80, 79, 77, 77, 71, 69, 64, 63, 60, 59, 53, 52, 50, 40, 39, 33, 33, 31, 23, 23, 20, 12, 3, 1, 
     Thus, the elements are deleted in their descenting order with the largest element deleted first.

   
     MinHeap:
    
     The Random No are :
82, 65, 38, 19, 81, 73, 60, 41, 82, 39, 56, 44, 74, 96, 50, 20, 87, 42, 51, 2, 37, 84, 99, 73, 42, 66, 71, 57, 32, 49, 
     The Deleted Elements according to deletion order are :
2, 19, 20, 32, 37, 38, 39, 41, 42, 42, 44, 49, 50, 51, 56, 57, 60, 65, 66, 71, 73, 73, 74, 81, 82, 82, 84, 87, 96, 99, 
     Thus, the elements are deleted in their ascending order with the smallest element deleted first.